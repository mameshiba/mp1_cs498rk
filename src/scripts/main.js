// Put your js code here
// Navbar Resize
window.addEventListener("scroll", scrollFunction);
function scrollFunction() {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
        document.getElementById("navbar").style.padding = "0px";
        document.getElementById("logo").style.fontSize = "15px";
        var x = document.querySelectorAll("#navbar-right a");
        for (let i = 0; i < x.length; i++) {
            x[i].style.fontSize = "15px";
        }
    
    } else if (document.body.scrollTop <= 80 && document.documentElement.scrollTop <= 80){
        document.getElementById("navbar").style.padding = "10px";
        document.getElementById("logo").style.fontSize = "35px";
        var x = document.querySelectorAll("#navbar-right a");
        for (let i = 0; i < x.length; i++) {
            x[i].style.fontSize = "25px";
        }
    }
}

window.addEventListener("scroll", pos_indicator);
var firstp = document.getElementById("video_container").offsetTop;  
var secondp = document.getElementById("second").offsetTop;  
var thirdp = document.getElementById("third").offsetTop;  
var fourthp = document.getElementById("fourth").offsetTop;  
function pos_indicator() {
    var topDist = document.documentElement.scrollTop;
    let active = document.getElementsByClassName("active");
    if (active.length > 0) {
        active[0].classList.remove("active");
    }
    if (topDist > firstp && topDist < secondp) {
        let barbtn = document.getElementById("logo");
        barbtn.classList.add("active");
    }
    else if (topDist > secondp && topDist < thirdp) {
        let barbtn = document.getElementById("about");
        barbtn.classList.add("active");
    }
    else if (topDist > thirdp && topDist < fourthp) {
        let barbtn = document.getElementById("members");
        barbtn.classList.add("active");
    }
    else if (topDist > fourthp) {
        let barbtn = document.getElementById("disco");
        barbtn.classList.add("active");
    }
}




// SlideShow
var slideIndex = 1;
showSlides(slideIndex);
var prevb = document.getElementsByClassName("prev");
var nextb = document.getElementsByClassName("next");
prevb[0].addEventListener("click", function() {nextSlide(-1);});
nextb[0].addEventListener("click", function() {nextSlide(1);});

function nextSlide(n) {
    showSlides(slideIndex += n);
}

function showSlides(n) {
    var slides = document.getElementsByClassName("mySlides");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slides[slideIndex-1].style.display = "block";
}  

// Modal Image
var albums = document.querySelectorAll(".flex-container img");

for (i = 0; i < albums.length; i++) {
    let srcName = albums[i].src;
    console.log(srcName);
    let modal = document.getElementById("modal" + srcName[srcName.length - 5]);
    console.log(modal.id)
    albums[i].addEventListener("click", function() {modal.style.display = "block";});

    // When the user clicks on <span> (x), close the modal
    let span = document.getElementById("close" + srcName[srcName.length - 5]);
    span.addEventListener("click", function() { modal.style.display = "none";
                                                console.log(modal.id);});
}



